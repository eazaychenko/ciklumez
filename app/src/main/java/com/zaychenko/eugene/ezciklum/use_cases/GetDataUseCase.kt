package com.zaychenko.eugene.ezciklum.use_cases

import com.ua.eugenezaychenko.attractez.di.scopes.OperationScope
import com.zaychenko.eugene.ezciklum.data.repository.IDataRepository
import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@OperationScope
class GetDataUseCase @Inject constructor(private val dataRepository: IDataRepository) : IGetDataUseCase {

    override fun invoke(cityName: String): Flow<ItemDataModel?> {
        return dataRepository.getWeatherData(cityName)
    }
}