package com.zaychenko.eugene.ezciklum.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ua.eugenezaychenko.attractez.di.scopes.OperationScope
import com.zaychenko.eugene.ciklum.di.ViewModelKey
import com.zaychenko.eugene.ciklum.di.WeatherViewModelFactory
import com.zaychenko.eugene.ezciklum.screens.MainViewModel
import com.zaychenko.eugene.ezciklum.use_cases.GetDataUseCase
import com.zaychenko.eugene.ezciklum.use_cases.GetWeatherHistoryUseCase
import com.zaychenko.eugene.ezciklum.use_cases.IGetDataUseCase
import com.zaychenko.eugene.ezciklum.use_cases.IGetWeatherHistoryUseCase
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class OperationModule {

    @Binds
    @OperationScope
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun provideMainViewModel(mainViewModel: MainViewModel): ViewModel

    @OperationScope
    @Binds
    abstract fun provideOperationViewModelFactory(factory: WeatherViewModelFactory): ViewModelProvider.Factory

    @OperationScope
    @Binds
    abstract fun provideGetDataUseCase(getDataUseCase: GetDataUseCase): IGetDataUseCase

    @OperationScope
    @Binds
    abstract fun provideGetWeatherHistoryUseCase(getWeatherHistoryUseCase: GetWeatherHistoryUseCase): IGetWeatherHistoryUseCase

}