package com.zaychenko.eugene.ezciklum.utils

import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import com.zaychenko.eugene.ezciklum.data.models.ItemModelUI

fun ItemDataModel.toModelUI(): ItemModelUI {
    return ItemModelUI(
        city = name,
        country = sys.country,
        temp = main.temp.toString(),
        windSpeed = wind.speed.toString(),
    )
}