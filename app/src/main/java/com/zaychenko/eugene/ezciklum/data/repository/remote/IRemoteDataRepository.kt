package com.zaychenko.eugene.ezciklum.data.repository.remote

import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import retrofit2.Response

interface IRemoteDataRepository {

    suspend fun getData(cityName: String): Response<ItemDataModel>

}