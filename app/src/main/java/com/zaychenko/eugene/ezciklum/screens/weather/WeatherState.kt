package com.zaychenko.eugene.ezciklum.screens.weather

import androidx.annotation.StringRes
import com.zaychenko.eugene.ezciklum.data.models.ItemModelUI


sealed class WeatherState

object InitWeatherState : WeatherState()
object InProgressWeatherState : WeatherState()
data class ResultWeatherState(val item: ItemModelUI) : WeatherState()
data class ErrorWeatherState(@StringRes val msgResId: Int) : WeatherState()
