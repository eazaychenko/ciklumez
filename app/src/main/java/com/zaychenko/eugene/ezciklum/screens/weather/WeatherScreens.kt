package com.zaychenko.eugene.ezciklum.screens.weather

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.zaychenko.eugene.ezciklum.R
import com.zaychenko.eugene.ezciklum.data.models.ItemModelUI
import com.zaychenko.eugene.ezciklum.screens.common.DataInCenter
import com.zaychenko.eugene.ezciklum.screens.common.ProgressDialog
import com.zaychenko.eugene.ezciklum.screens.common.RequestInput
import com.zaychenko.eugene.ezciklum.screens.common.ResultDataText
import com.zaychenko.eugene.ezciklum.ui.theme.Purple700

@Composable
fun WeatherScreens(
    state: WeatherState,
    onWeatherRequest: (String) -> Unit,
) {

    when (state) {
        is InitWeatherState -> BaseWeatherUI(onWeatherRequest, { DataInCenter { InitCenteredContent() } })
        is InProgressWeatherState -> BaseWeatherUI(onWeatherRequest, { ProgressDialog() })
        is ResultWeatherState -> {
            BaseWeatherUI(onWeatherRequest, {
                DataInCenter { ResultUI(state.item) }
            })
        }
        is ErrorWeatherState -> BaseWeatherUI(onWeatherRequest, {
            DataInCenter { ErrorCenteredContent(stringResource(state.msgResId)) }
        })
    }
}

@Composable
fun BaseWeatherUI(onWeatherRequest: (String) -> Unit, content: @Composable () -> Unit) {
    Column(modifier = Modifier.padding(dimensionResource(R.dimen.padding_default))) {
        RequestInput(onWeatherRequest = onWeatherRequest, hint = stringResource(R.string.hint))
    }
    content()
}


@Composable
fun ResultUI(item: ItemModelUI) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(dimensionResource(R.dimen.padding_default)),
        elevation = dimensionResource(R.dimen.default_elevation),
    ) {
        Column(modifier = Modifier.padding(dimensionResource(R.dimen.padding_default))) {
            Text(
                "${stringResource(R.string.result_main_title)} ${item.city} (${item.country})",
                style = TextStyle(fontWeight = FontWeight.Bold, color = Purple700, fontSize = 20.sp),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(dimensionResource(R.dimen.padding_default)),
            )
            ResultDataText(stringResource(R.string.result_title_temp_prefix), item.temp, stringResource(R.string.result_title_temp_suffix))
            ResultDataText(stringResource(R.string.result_title_wind_prefix), item.windSpeed, stringResource(R.string.result_title_wind_suffix))
        }
    }

}

@Composable
fun InitCenteredContent() {
    Text(stringResource(R.string.placeholder_no_data), color = Color.Gray)
}

@Composable
fun ErrorCenteredContent(errorMsg: String) {
    Text(errorMsg, color = Color.Black, modifier = Modifier.padding(dimensionResource(R.dimen.padding_default)), textAlign = TextAlign.Center)
}
