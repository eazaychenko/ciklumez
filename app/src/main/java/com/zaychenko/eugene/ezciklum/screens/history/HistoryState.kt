package com.zaychenko.eugene.ezciklum.screens.history

import com.zaychenko.eugene.ezciklum.data.models.ItemModelUI

sealed class HistoryState

object InitHistoryState : HistoryState()
data class ResultHistoryState(val itemModelList: List<ItemModelUI>) : HistoryState()
object EmptyHistoryState : HistoryState()