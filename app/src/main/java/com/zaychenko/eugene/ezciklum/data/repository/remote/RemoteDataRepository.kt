package com.zaychenko.eugene.ezciklum.data.repository.remote

import android.content.Context
import com.zaychenko.eugene.ezciklum.data.ICiklumService
import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import com.zaychenko.eugene.ezciklum.utils.NoInternetConnectionException
import com.zaychenko.eugene.ezciklum.utils.isOnline
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataRepository @Inject constructor(
    private val context: Context,
    private val service: ICiklumService,
    private val dispatcherIO: CoroutineDispatcher
) : IRemoteDataRepository {

    override suspend fun getData(cityName: String): Response<ItemDataModel> = withContext(dispatcherIO) {
        if (!context.isOnline()) throw NoInternetConnectionException()
        return@withContext service.getWeather(cityName)
    }

}