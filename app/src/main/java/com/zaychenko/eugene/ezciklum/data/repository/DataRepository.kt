package com.zaychenko.eugene.ezciklum.data.repository

import com.zaychenko.eugene.ezciklum.data.repository.remote.IRemoteDataRepository
import com.zaychenko.eugene.ezciklum.utils.IPrefManager
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor(
    private val remoteDataRepository: IRemoteDataRepository,
    private val prefManager: IPrefManager,
) : IDataRepository {


    override fun getWeatherData(cityName: String) = flow {
        val item = remoteDataRepository.getData(cityName).body()
        if (item?.cod == RESULT_SUCCESS) {
            prefManager.addResponse(item)
        }
        emit(item)
    }

    override fun getWeatherRequestHistory() = flow {
        emit(prefManager.getWeatherHistoryList())
    }

    companion object {
        const val RESULT_SUCCESS = 200L
    }
}