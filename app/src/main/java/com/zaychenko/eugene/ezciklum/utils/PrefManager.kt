package com.zaychenko.eugene.ezciklum.utils

import android.content.SharedPreferences
import com.google.gson.Gson
import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import com.zaychenko.eugene.ezciklum.utils.Const.Companion.PREF_KEY
import javax.inject.Inject

class PrefManager @Inject constructor(
    private val sp: SharedPreferences,
    private val gson: Gson,
) : IPrefManager {

    private val weatherHistoryList = mutableListOf<ItemDataModel>()

    override fun addResponse(itemDataModel: ItemDataModel) {
        with(sp.edit()) {
            weatherHistoryList.add(itemDataModel)
            putString(PREF_KEY, gson.toJson(weatherHistoryList))
            commit()
        }
    }

    override fun getWeatherHistoryList(): List<ItemDataModel> {
        if (weatherHistoryList.isEmpty()) {
            weatherHistoryList.addAll(getListFromPref())
        }
        return weatherHistoryList
    }

    private fun getListFromPref(): List<ItemDataModel> {
        return sp.getString(PREF_KEY, null)?.let { json ->
            return@let gson.fromJson(json, Array<ItemDataModel>::class.java).toList()
        } ?: emptyList()
    }

}