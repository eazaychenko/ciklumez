package com.zaychenko.eugene.ezciklum.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ua.eugenezaychenko.attractez.di.scopes.OperationScope
import com.zaychenko.eugene.ezciklum.R
import com.zaychenko.eugene.ezciklum.screens.common.*
import com.zaychenko.eugene.ezciklum.screens.history.EmptyHistoryState
import com.zaychenko.eugene.ezciklum.screens.history.HistoryState
import com.zaychenko.eugene.ezciklum.screens.history.InitHistoryState
import com.zaychenko.eugene.ezciklum.screens.history.ResultHistoryState
import com.zaychenko.eugene.ezciklum.screens.weather.*
import com.zaychenko.eugene.ezciklum.use_cases.IGetDataUseCase
import com.zaychenko.eugene.ezciklum.use_cases.IGetWeatherHistoryUseCase
import com.zaychenko.eugene.ezciklum.utils.NoInternetConnectionException
import com.zaychenko.eugene.ezciklum.utils.toModelUI
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@OperationScope
class MainViewModel @Inject constructor(
    private val getDataUseCase: IGetDataUseCase,
    private val getWeatherHistoryUseCase: IGetWeatherHistoryUseCase
) : ViewModel() {

    private val _weatherState = MutableStateFlow<WeatherState>(InitWeatherState)
    val weatherState: StateFlow<WeatherState> = _weatherState.asStateFlow()

    private val _historyState = MutableStateFlow<HistoryState>(InitHistoryState)
    val historyState: StateFlow<HistoryState> = _historyState.asStateFlow()

    init {
        viewModelScope.launch { updateHistoryScreen() }
    }

    fun getWeather(city: String) = viewModelScope.launch {
        if (city.trim().isEmpty()) {
            _weatherState.emit(ErrorWeatherState(R.string.error_empty_city))
            return@launch
        }

        try {
            _weatherState.emit(InProgressWeatherState)
            getDataUseCase.invoke(city).collect { item ->
                if (item == null) {
                    _weatherState.emit(ErrorWeatherState(R.string.error_empty_response))
                } else {
                    updateHistoryScreen()
                    _weatherState.emit(ResultWeatherState(item.toModelUI()))
                }
            }
        } catch (e: Exception) {
            if (e is NoInternetConnectionException) {
                _weatherState.emit(ErrorWeatherState(R.string.error_no_internet_connection))
            } else {
                _weatherState.emit(ErrorWeatherState(R.string.error_server_error))
            }
        }

    }

    private suspend fun updateHistoryScreen() {
        getWeatherHistoryUseCase.invoke()
            .collect { itemList ->
                if (itemList.isEmpty()) {
                    _historyState.emit(EmptyHistoryState)
                } else {
                    _historyState.emit(ResultHistoryState(itemModelList = itemList.map { it.toModelUI() }))
                }

            }
    }

}