package com.zaychenko.eugene.ezciklum

import android.app.Application
import com.zaychenko.eugene.ezciklum.di.components.AppComponent
import com.zaychenko.eugene.ezciklum.di.components.DaggerAppComponent
import com.zaychenko.eugene.ezciklum.di.components.OperationComponent

class CiklumApp : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .app(this)
            .build()
        appComponent.inject(this)

    }

    companion object {

        lateinit var appComponent: AppComponent

        var operationComponent: OperationComponent? = null
            get() {
                if (field == null)
                    field = appComponent.provideOperationComponent()
                return field
            }

    }

}