package com.zaychenko.eugene.ezciklum.data

import androidx.annotation.WorkerThread
import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import com.zaychenko.eugene.ezciklum.utils.Const.Companion.API_KEY
import com.zaychenko.eugene.ezciklum.utils.Const.Companion.UNITS
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ICiklumService {

    companion object {
        const val BASE_URL_SERVICE = "https://api.openweathermap.org/data/2.5/"
    }

    @WorkerThread
    @GET("weather")
    suspend fun getWeather(
        @Query("q") city: String,
        @Query("appid") appId: String = API_KEY,
        @Query("units") units: String = UNITS,
    ): Response<ItemDataModel>

}