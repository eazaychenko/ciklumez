package com.zaychenko.eugene.ezciklum.di.module

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.zaychenko.eugene.ezciklum.BuildConfig
import com.zaychenko.eugene.ezciklum.data.ICiklumService
import com.zaychenko.eugene.ezciklum.data.repository.DataRepository
import com.zaychenko.eugene.ezciklum.data.repository.IDataRepository
import com.zaychenko.eugene.ezciklum.data.repository.remote.IRemoteDataRepository
import com.zaychenko.eugene.ezciklum.data.repository.remote.RemoteDataRepository
import com.zaychenko.eugene.ezciklum.utils.Const.Companion.PREF_NAME
import com.zaychenko.eugene.ezciklum.utils.IPrefManager
import com.zaychenko.eugene.ezciklum.utils.PrefManager
import dagger.Binds
import dagger.Lazy
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun provideDataRepository(dataRepository: DataRepository): IDataRepository

    @Binds
    @Singleton
    abstract fun provideRemoteDataRepository(remoteDataRepository: RemoteDataRepository): IRemoteDataRepository

    @Binds
    @Singleton
    abstract fun providePrefManager(prefManager: PrefManager): IPrefManager

    @Module
    companion object {

        private const val timeOut = 20L

        @JvmStatic
        @Provides
        @Singleton
        fun provideGson() = Gson()

        @Provides
        @Singleton
        fun provideClient(): OkHttpClient {
            return OkHttpClient.Builder().apply {
                readTimeout(timeOut, TimeUnit.SECONDS)
                connectTimeout(timeOut, TimeUnit.SECONDS)
                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                        addInterceptor(this)
                    }
                }
            }.build()
        }

        @Provides
        @Singleton
        fun provideRetrofit(client: Lazy<OkHttpClient>, gson: Gson): Retrofit {
            return Retrofit.Builder()
                .baseUrl(ICiklumService.BASE_URL_SERVICE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callFactory { client.get().newCall(it) }
                .build()
        }

        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): ICiklumService {
            return retrofit.create(ICiklumService::class.java)
        }

        @Provides
        @Singleton
        fun provideCoroutineDispatcherIO(): CoroutineDispatcher {
            return Dispatchers.IO
        }

        @Provides
        @Singleton
        fun provideSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }
    }


}