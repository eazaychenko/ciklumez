package com.zaychenko.eugene.ezciklum.data.models

data class ItemModelUI constructor(
    val city: String,
    val country: String,
    val temp: String,
    val windSpeed: String,
)