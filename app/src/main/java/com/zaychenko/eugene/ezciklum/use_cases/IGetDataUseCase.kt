package com.zaychenko.eugene.ezciklum.use_cases

import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import kotlinx.coroutines.flow.Flow

interface IGetDataUseCase {
    operator fun invoke(cityName: String): Flow<ItemDataModel?>
}