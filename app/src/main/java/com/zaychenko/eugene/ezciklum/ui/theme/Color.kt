package com.zaychenko.eugene.ezciklum.ui.theme

import androidx.compose.ui.graphics.Color

/*
100% — FF
95% — F2
90% — E6

85% — D9

80% — CC
75% — BF
70% — B3
65% — A6
60% — 99
55% — 8C
50% — 80
45% — 73
40% — 66
35% — 59
30% — 4D
25% — 40
20% — 33
15% — 26
10% — 1A
5% — 0D
0% — 00
*/

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Gray60 = Color(0x99001318)
val ColorTitle = Color(0xFF4552B8)