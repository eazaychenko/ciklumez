package com.zaychenko.eugene.ezciklum.use_cases

import com.ua.eugenezaychenko.attractez.di.scopes.OperationScope
import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import com.zaychenko.eugene.ezciklum.data.repository.IDataRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@OperationScope
class GetWeatherHistoryUseCase @Inject constructor(private val dataRepository: IDataRepository) : IGetWeatherHistoryUseCase {

    override fun invoke(): Flow<List<ItemDataModel>> {
        return dataRepository.getWeatherRequestHistory()
    }

}