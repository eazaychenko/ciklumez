package com.zaychenko.eugene.ezciklum.screens.history

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.zaychenko.eugene.ezciklum.R
import com.zaychenko.eugene.ezciklum.data.models.ItemModelUI
import com.zaychenko.eugene.ezciklum.screens.common.DataInCenter
import com.zaychenko.eugene.ezciklum.screens.weather.ResultUI


@Composable
fun HistoryScreen(state: HistoryState) {
    when (state) {
        is InitHistoryState -> EmptyHistoryScreen()
        is EmptyHistoryState -> EmptyHistoryScreen()
        is ResultHistoryState -> HistoryContent(state.itemModelList)
    }

}

@Composable
private fun EmptyHistoryScreen() {
    DataInCenter {
        Text(
            stringResource(R.string.history_empty_screen_msg),
            color = Color.Gray,
            modifier = Modifier.padding(dimensionResource(R.dimen.padding_default)),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun HistoryContent(itemModelList: List<ItemModelUI>) {
    LazyColumn {
        items(itemModelList) { item ->
            ResultUI(item)
        }
    }
}


@Preview(showBackground = true)
@Composable
fun PreviewHistoryContent() {
    val list = mutableListOf<ItemModelUI>()
    list.add(ItemModelUI(city = "Odessa", country = "UA", temp = "24", windSpeed = "5"))
    list.add(ItemModelUI(city = "London", country = "GB", temp = "14", windSpeed = "15"))
    list.add(ItemModelUI(city = "Kharkiv", country = "UA", temp = "20", windSpeed = "3"))
    list.add(ItemModelUI(city = "Minsk", country = "BY", temp = "12", windSpeed = "4"))
    list.add(ItemModelUI(city = "Mykolayiv", country = "UA", temp = "18", windSpeed = "7"))
    HistoryContent(list)
}