package com.zaychenko.eugene.ezciklum

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.zaychenko.eugene.ezciklum.screens.MainViewModel
import com.zaychenko.eugene.ezciklum.screens.common.BottomNavigationBar
import com.zaychenko.eugene.ezciklum.screens.common.BottomNavigationScreens
import com.zaychenko.eugene.ezciklum.screens.history.HistoryScreen
import com.zaychenko.eugene.ezciklum.screens.history.HistoryState
import com.zaychenko.eugene.ezciklum.screens.weather.WeatherScreens
import com.zaychenko.eugene.ezciklum.screens.weather.WeatherState
import com.zaychenko.eugene.ezciklum.ui.theme.EzCiklumTheme
import com.zaychenko.eugene.ezciklum.utils.launchWhenStarted
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class MainActivity : ComponentActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CiklumApp.operationComponent?.inject(this)
        setContent {
            EzCiklumTheme {
                Surface {
                    MainScreen()
                }
            }
        }
    }

    @Composable
    private fun MainScreen() {
        val navController = rememberNavController()
        val bottomNavigationItems = listOf(
            BottomNavigationScreens.Request,
            BottomNavigationScreens.History,
        )

        Scaffold(bottomBar = { BottomNavigationBar(navController, bottomNavigationItems) }) { innerPadding ->
            Box(modifier = Modifier.padding(innerPadding)) {
                MainScreenNavigationConfigurations(navController)
            }
        }
    }

    @Composable
    private fun WeatherActivityScreen() {
        var currentState: WeatherState by remember { mutableStateOf(viewModel.weatherState.value) }
        viewModel.weatherState
            .onEach { currentState = it }
            .launchWhenStarted(lifecycleScope)

        WeatherScreens(
            state = currentState,
            onWeatherRequest = viewModel::getWeather
        )
    }

    @Composable
    private fun HistoryActivityScreen() {
        var currentState: HistoryState by remember { mutableStateOf(viewModel.historyState.value) }
        viewModel.historyState
            .onEach { currentState = it }
            .launchWhenStarted(lifecycleScope)

        HistoryScreen(state = currentState)
    }

    @Composable
    private fun MainScreenNavigationConfigurations(navController: NavHostController) {
        NavHost(navController, startDestination = BottomNavigationScreens.Request.route) {
            composable(BottomNavigationScreens.Request.route) {
                WeatherActivityScreen()
            }
            composable(BottomNavigationScreens.History.route) {
                HistoryActivityScreen()
            }
        }
    }


}