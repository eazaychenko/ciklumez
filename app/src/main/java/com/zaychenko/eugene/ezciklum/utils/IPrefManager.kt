package com.zaychenko.eugene.ezciklum.utils

import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel

interface IPrefManager {

    fun addResponse(itemDataModel: ItemDataModel)

    fun getWeatherHistoryList(): List<ItemDataModel>

}