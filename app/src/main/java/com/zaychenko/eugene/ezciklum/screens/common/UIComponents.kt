package com.zaychenko.eugene.ezciklum.screens.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.zaychenko.eugene.ezciklum.R
import com.zaychenko.eugene.ezciklum.ui.theme.Gray60
import com.zaychenko.eugene.ezciklum.utils.noRippleClickable


@Composable
fun RequestInput(onWeatherRequest: (String) -> Unit, hint: String) {
    val (text, setText) = remember { mutableStateOf("") }
    Column {
        Row(
            Modifier
                .padding(horizontal = dimensionResource(R.dimen.padding_default))
                .padding(top = dimensionResource(R.dimen.padding_default))
        ) {
            RequestInputTextField(
                text = text,
                hint = hint,
                onTextChange = setText,
                modifier = Modifier
                    .weight(1f)
                    .padding(end = dimensionResource(R.dimen.padding_very_small)),
            )
            RequestButton(
                onClick = {
                    onWeatherRequest(text) // send onItemComplete event up
                    setText("") // clear the internal text
                },
                text = stringResource(R.string.request),
                modifier = Modifier.align(Alignment.CenterVertically),
                enabled = text.isNotBlank() // enable if text is not blank
            )
        }
    }
}

@Composable
fun RequestInputTextField(text: String, hint: String, onTextChange: (String) -> Unit, modifier: Modifier) {
    RequestInputText(text, hint, onTextChange, modifier)
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun RequestInputText(
    text: String,
    hint: String,
    onTextChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    onImeAction: () -> Unit = {}
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    TextField(
        value = text,
        placeholder = { Text(hint) },
        onValueChange = onTextChange,
        colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.Transparent),
        maxLines = 1,
        keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Done),
        keyboardActions = KeyboardActions(onDone = {
            onImeAction()
            keyboardController?.hide()
        }),
        modifier = modifier
    )
}

@Composable
fun RequestButton(
    onClick: () -> Unit,
    text: String,
    modifier: Modifier = Modifier,
    enabled: Boolean = true
) {
    TextButton(
        onClick = onClick,
        shape = CircleShape,
        enabled = enabled,
        modifier = modifier
    ) {
        Text(text)
    }
}

@Composable
fun DataInCenter(content: @Composable () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        content()
    }
}

@Composable
fun ProgressDialog() {
    Column(
        modifier = Modifier
            .background(Gray60)
            .noRippleClickable {}
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ResultDataText(prefix: String, value: String, suffix: String) {
    Text(
        "$prefix: $value $suffix",
        style = TextStyle(fontWeight = FontWeight.Normal, color = Color.Black, fontSize = 18.sp),
        textAlign = TextAlign.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(dimensionResource(R.dimen.padding_ultra_small)),
    )
}

@Preview
@Composable
fun PreviewRequestInputRow() = RequestInput(hint = stringResource(R.string.hint), onWeatherRequest = {})