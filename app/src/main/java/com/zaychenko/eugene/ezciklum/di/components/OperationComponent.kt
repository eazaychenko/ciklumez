package com.zaychenko.eugene.ezciklum.di.components

import com.ua.eugenezaychenko.attractez.di.scopes.OperationScope
import com.zaychenko.eugene.ezciklum.MainActivity
import com.zaychenko.eugene.ezciklum.di.module.OperationModule
import dagger.Subcomponent

@Subcomponent(modules = [OperationModule::class])
@OperationScope
interface OperationComponent {

    fun inject(mainActivity: MainActivity)

}