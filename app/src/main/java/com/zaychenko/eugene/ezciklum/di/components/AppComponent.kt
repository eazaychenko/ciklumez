package com.zaychenko.eugene.ezciklum.di.components

import android.app.Application

import com.zaychenko.eugene.ezciklum.CiklumApp
import com.zaychenko.eugene.ezciklum.di.module.AndroidModule
import com.zaychenko.eugene.ezciklum.di.module.DataModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        AndroidModule::class,
        DataModule::class
    ]
)

@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun app(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(app: CiklumApp)

    fun provideOperationComponent(): OperationComponent

}