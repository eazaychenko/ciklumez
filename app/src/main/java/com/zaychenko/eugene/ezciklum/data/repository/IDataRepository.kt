package com.zaychenko.eugene.ezciklum.data.repository

import com.zaychenko.eugene.ezciklum.data.models.ItemDataModel
import kotlinx.coroutines.flow.Flow

interface IDataRepository {

    fun getWeatherData(cityName: String): Flow<ItemDataModel?>

    fun getWeatherRequestHistory(): Flow<List<ItemDataModel>>

}